#!/usr/bin/perl -wT
use strict;

my $lno = 550;
my @vars = ();

while (my $line = <>) {
	chomp $line;
	$line = uc $line;
	next unless ($line =~ /^([A-Z][A-Z0-9]*)\s+EQU\s+#?(\d+)$/);
	push @vars, "$1=$2";
	if (scalar(@vars) == 4) {
		print_vars(@vars);
		@vars = ();
	}
}
print_vars(@vars);
print "$lno RETURN\n";

sub print_vars {
	return if (scalar(@_) == 0);
	print "$lno ".join(":", @_)."\n";
	$lno++;
}
