#!/usr/bin/perl -wT
use strict;

sub dump_sprite ($$);
sub to_numbers($);

my $prefix = shift @ARGV;

my @mask0;
my @data0;

while (my $line = <>) {
	chomp $line;
	my ($m0,$d0) = split(/\s+/, $line);
	die "bad mask" unless ($m0 =~ /^(([01]{8})+)$/);
	$m0 = $1;  # de-taint
	die "bad data" unless ($d0 =~ /^(([01]{8})+)$/);
	$d0 = $1;  # de-taint
	$m0 .= "11111111";
	$d0 .= "00000000";
	push @mask0, to_numbers($m0);
	push @data0, to_numbers($d0);
}

die "mismatched mask/data count" if scalar(@mask0) != scalar(@data0);
die "bad data count" if scalar(@data0) != 21*4;

print "${prefix}0\n";
dump_sprite(\@mask0, \@data0);
print "\n";

sub dump_sprite ($$) {
	my ($m,$d) = @_;
	my @out = ();
	while (@$m) {
		my @out = ();
		push @out, shift @$m;
		push @out, shift @$m;
		push @out, shift @$d;
		push @out, shift @$d;
		print "\t\tfcb ".join(",", map { sprintf "\$\%02x", $_ } @out)."\n";
	}
}

sub to_numbers($) {
	my $data = shift;
	my @nums = ();
	while ($data) {
		push @nums, eval("0b".substr($data,0,8));
		$data = substr($data,8);
	}
	return @nums;
}
