gscreen_ptr	equ $00ba

; PIA0
reg_pia0_ddra           equ     $ff00
reg_pia0_pdra           equ     $ff00
reg_pia0_cra            equ     $ff01
reg_pia0_ddrb           equ     $ff02
reg_pia0_pdrb           equ     $ff02
reg_pia0_crb            equ     $ff03

; PIA1
reg_pia1_ddra           equ     $ff20
reg_pia1_pdra           equ     $ff20
reg_pia1_cra            equ     $ff21
reg_pia1_ddrb           equ     $ff22
reg_pia1_pdrb           equ     $ff22
reg_pia1_crb            equ     $ff23

sprite_nlines	equ 21
sizeof_sprite	equ sprite_nlines*8

		org $8000-sizeof_ALL
		setdp 0

start

		export IX	; Initialise
		export QP	; Q*Bert position
		export CP	; Coilly position
		export BP	; Splodge position
		export DQ	; Draw Q*Bert
		export DC	; Draw Coilly
		export DB	; Draw Splodge
		export IQ	; Invert Q*Bert
		export RQ	; Redraw Q*Bert
		export RC	; Redraw Coilly
		export RB	; Redraw Splodge
		export UE	; Undraw everything
		export MQ	; Mangle Q*Bert
		export SL	; Slow down text
		export SB	; Score (5 bytes)
		export LV	; Level
		export PS	; Print Score
		export PL	; Print Level
		export CS	; Copy Screen
		export U1	; Undraw Circle 1
		export U2	; Undraw Circle 2

QP		fdb 0
CP		fdb 0
BP		fdb 0
old_QP		fdb 0
old_CP		fdb 0
old_BP		fdb 0

; Score bytes
SB		fcb 0,0,0,0,0
; Level
LV		fcb 0
stmp		fdb 0

; Redraw Splodge
RB		ldd old_BP
		jsr undraw_sprite
		; fall through to DB
; Draw Splodge
DB		ldd BP
		std old_BP
		ldu #splodge0
		bra draw_sprite

; Redraw Coilly
RC		ldd old_CP
		jsr undraw_sprite
		; fall through to DC
; Draw Coilly
DC		ldd CP
		std old_CP
		ldu #coilly0
		bra draw_sprite

; Redraw Q*Bert
RQ		ldd old_QP
		jsr undraw_sprite
		; fall through to DQ
; Draw Q*Bert
DQ		ldd QP
		std old_QP
		ldu #qbert0
		bra draw_sprite

; Undraw circles
U2		ldd #$a874
		bra >
U1		ldd #$3074
!		bsr get_screen_pos
		ldu #circle
		ldb #9
		bra 10F

draw_sprite
		bsr get_screen_pos

		; 21 lines
		ldb #21
10		pshs b
!		pulu d
		anda ,x
		andb 1,x
		addd ,u++
		std ,x
		pulu d
		anda 2,x
		andb 3,x
		addd ,u++
		std 2,x
		leax 32,x
		;
		dec ,s
		bne <
		puls b,pc

get_screen_pos	pshs a
		lda #32
		mul
		ldx gscreen_ptr
		leax d,x
		puls b
		lsrb
		lsrb
		bcc >
		leau 3*sizeof_sprite,u
!		lsrb
		bcc >
		leau 6*sizeof_sprite,u
!		abx
		rts

; Undraw everything
UE		ldd QP
		bsr undraw_sprite
		ldd CP
		bsr undraw_sprite
		ldd BP
		bsr undraw_sprite
		ldd #$326e
		bsr undraw_sprite
		ldd #$aa6e
		; fall through to undraw_sprite

undraw_sprite
		bsr get_screen_pos
		leay 6144,x
		ldb #21
		pshs b
!		ldd ,y
		std ,x
		ldd 2,y
		std 2,x
		leax 32,x
		leay 32,y
		dec ,s
		bne <
		puls b,pc

; Invert Q*Bert
IQ		ldd QP
		ldu #qbert0
		; fall through to invert_sprite

invert_sprite
		bsr get_screen_pos

		; 21 lines
		ldb #21
		pshs b
!		pulu d,y
		eora ,x
		eorb 1,x
		coma
		comb
		std ,x
		pulu d,y
		eora 2,x
		eorb 3,x
		coma
		comb
		std 2,x
		leax 32,x
		;
		dec ,s
		bne <
		puls b,pc

; Initialise
IX		ldx #gfx_start
		ldb #9		; shift 3 sprites 3 times
		pshs b
		;
20		ldb #sprite_nlines
		pshs b
		;
10		bsr rolline
		leax 2,x
		bsr rolline
		leax 6,x
		dec ,s
		bne 10B
		leas 1,s
		dec ,s
		bne 20B
		puls b,pc

rolline		leay 3*sizeof_sprite,x
		ldd 4,x
		bsr rolbyte
		stb 5,y
		ldb 4,x
		lda 1,x
		bsr rolbyte
		stb 4,y
		ldd ,x
		bsr rolbyte
		stb 1,y
		ldb ,x
		lda 5,x
		bsr rolbyte
		stb ,y
		rts

rolbyte		lsra
		rorb
		lsra
		rorb
		rts

; Mangle Q*Bert
MQ		ldd QP
		jsr get_screen_pos
		ldb #10
		pshs b
!		lsr ,x
		lsl 1,x
		lsr 2,x
		lsl 3,x
		leax 32,x
		lsl ,x
		lsr 1,x
		lsl 2,x
		lsr 3,x
		leax 32,x
		dec ,s
		bne <
		puls b,pc

; Slow text
SL		pshs a,b
		ldd #$fd40
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra
		beq >
		sync
		sync
		sync
!		lda #$ff
		sta reg_pia0_pdrb
		puls a,b,pc

; Print score
PS		ldd #$081e
		jsr get_screen_pos
		ldu #SB
		ldb #5
		pshs b
!		lda ,u+
		bsr print_num
		dec ,s
		bne <
		puls b,pc

print_num	ldb #11
		pshs b,u
		mul
		ldu #numbers
		leau d,u
!		pulu a
		sta ,x
		leax 32,x
		dec ,s
		bne <
		leax -11*32+1,x
		puls b,u,pc

; Print level
PL		ldd #$1846
		jsr get_screen_pos
		lda LV
		bra print_num

; Copy screen
CS		pshs cc,dp
		orcc #$50
		sts stmp
		ldu gscreen_ptr
		leas 6139,u
		sts mod_screen_end
		leas 12,s
!		pulu dp,a,b,x,y
		pshs dp,a,b,x,y
		leas 14,s
mod_screen_end	equ *+2
		cmpu #$0000
		blo <
		lds >stmp
		puls cc,dp,pc

numbers

		fcb %11111100	; 0
		fcb %11000100
		fcb %10100100
		fcb %10010100
		fcb %10001100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100

		fcb %00000100	; 1
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100

		fcb %11111100	; 2
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %11111100
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %11111100

		fcb %11111100	; 3
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %11111100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %11111100

		fcb %10000100	; 4
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100

		fcb %11111100	; 5
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %11111100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %11111100

		fcb %10000000	; 6
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %11111100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100

		fcb %11111100	; 7
		fcb %00001000
		fcb %00010000
		fcb %00100000
		fcb %01000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000
		fcb %10000000

		fcb %11111100	; 8
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100

		fcb %11111100	; 9
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %10000100
		fcb %11111100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100
		fcb %00000100

; thankfully the teleport pads are in the same byte-relative place,
; so only need one of these!
circle
		fcb %11111111,%11110111,0,0,%11111111,$ff,0,0
		fcb %11111110,%00000000,0,0,%00111111,$ff,0,0
		fcb %11111100,%11111111,0,0,%10011111,$ff,0,0
		fcb %11111011,%11111111,0,0,%11101111,$ff,0,0
		fcb %11110011,%11111111,0,0,%11100111,$ff,0,0
		fcb %11111011,%11111111,0,0,%11101111,$ff,0,0
		fcb %11111100,%11111111,0,0,%10011111,$ff,0,0
		fcb %11111110,%00000000,0,0,%00111111,$ff,0,0
		fcb %11111111,%11110111,0,0,%11111111,$ff,0,0

gfx_start
		include "qbert2-gfx.s"

		rmb 3*sizeof_sprite
		rmb 3*sizeof_sprite
		rmb 3*sizeof_sprite

sizeof_ALL	equ *-start
