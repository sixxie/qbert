#!/usr/bin/perl -wT
use strict;

my $lno = 600;

while (my $line = uc <>) {
	chomp $line;
	next unless ($line =~ /^:([0-9A-F]{2})([0-9A-F]{4})[0-9A-F]{2}(.*)([0-9A-F]{2})$/);
	my ($len,$addr,$bytes,$sum) = ($1,$2,$3,$4);
	last if $len eq '00';
	$sum = uc sprintf "\%02x", (256-hex($sum)) & 0xff;
	print "$lno DATA $addr,$bytes,$sum\n";
	$lno++;
}
print "$lno DATA 0\n";
