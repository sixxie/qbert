# Q*Bert+
# Copyright (c) 1984 Philip Thompson
# Machine Code enhancements by sixxie, 2017

.PHONY: all
all: qbert2.asc

####

ASM6809 = asm6809 -v
CLEAN =

####

qbert2-bin.asc: qbert2-bin.hex
	./hex2bas.pl 

CLEAN += qbert2-bin.asc

qbert2-gfx.s: qbert.txt splodge.txt coilly.txt
	./makegfx.pl qbert qbert.txt > $@
	./makegfx.pl splodge splodge.txt >> $@
	./makegfx.pl coilly coilly.txt >> $@

CLEAN += qbert2-gfx.s

qbert2-bin.hex: qbert2-bin.s qbert2-gfx.s
	$(ASM6809) -H -l qbert2-bin.lis -E qbert2-bin.exp -o $@ $<

CLEAN += qbert2-bin.hex qbert2-bin.lis qbert2-bin.exp

qbert2.asc: qbert2-bas.asc qbert2-bin.hex
	cat qbert2-bas.asc > $@
	./sym2bas.pl qbert2-bin.exp >> $@
	./hex2bas.pl qbert2-bin.hex >> $@

CLEAN += qbert2.asc

####

.PHONY: clean
clean:
	rm -f $(CLEAN)
