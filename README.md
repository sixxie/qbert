# Q\*Bert Redux

The original version of Q\*Bert by Philip Thompson was written in BASIC
and published as a listing in Dragon User, January 1985.

This version adds some Machine Code helper routines to speed things up a bit,
and fixes a couple of bugs along the way.
